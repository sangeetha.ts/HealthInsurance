package com.health.insurance.controller;

import com.health.insurance.model.CurrentHealth;
import com.health.insurance.model.Gender;
import com.health.insurance.model.Habits;
import com.health.insurance.model.InsurancePremiumDetails;
import com.health.insurance.service.InsurancePremiumService;
import com.health.insurance.service.InsurancePremiumServiceImpl;

public class InsurancePremium {
	
	public static void main(String[] args) {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		
		Habits habits = new Habits();
		CurrentHealth currentHealth = new CurrentHealth();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
	
		habits.setAlcohol(true);
		habits.setDailyExercise(true);
		
		currentHealth.setOverweight(true);
		
		details.setName("Norman Gomes");
		details.setGender(Gender.MALE);
		details.setAge(34);
		
		details.setCurrentHealth(currentHealth);
		details.setHabits(habits);
		
		System.out.println(insurancePremiumService.calculateInsurancePremium(details));
	}

}

package com.health.insurance.model;

public enum Gender {
	MALE,
	FEMALE;
}

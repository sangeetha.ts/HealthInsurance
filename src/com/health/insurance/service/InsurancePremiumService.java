package com.health.insurance.service;

import com.health.insurance.model.InsurancePremiumDetails;

public interface InsurancePremiumService {
	
	public double calculateInsurancePremium(InsurancePremiumDetails insurancePremiumDetails);

}

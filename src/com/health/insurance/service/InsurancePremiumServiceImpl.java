package com.health.insurance.service;

import com.health.insurance.model.CurrentHealth;
import com.health.insurance.model.Habits;
import com.health.insurance.model.InsurancePremiumDetails;

public class InsurancePremiumServiceImpl implements InsurancePremiumService {
	
	private static int basePremium = 5000; 
	
	@Override
	public double calculateInsurancePremium(InsurancePremiumDetails insurancePremiumDetails) {
		
		double totalPremiumAmt = 0;
		int age = insurancePremiumDetails.getAge();
		
		if(age < 18) {
			totalPremiumAmt = basePremium;
		} else if(age > 18 && age < 25) {
			totalPremiumAmt = getPremiumAmt(1);
		} else if(age > 25 && age < 30) {
			totalPremiumAmt = getPremiumAmt(2);
		} else if(age > 30 && age < 35) {
			totalPremiumAmt = getPremiumAmt(3);
		} else if(age > 35 && age < 40) {
			totalPremiumAmt = getPremiumAmt(4);			
		} else if(age > 40) {
			totalPremiumAmt = getPremiumAmt(5);
			int ageDiff = age - 40;
			int percent = ageDiff/5;
			
			for(int i = 1; i < percent; i++) {
				totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt*20)/100);
			}
		}
		
		if(insurancePremiumDetails.getGender().name().equals("MALE")) {
			totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt*2)/100);			
		}
		
		totalPremiumAmt = calculateCurrentHealthPremium(insurancePremiumDetails.getCurrentHealth(), totalPremiumAmt);
		
		totalPremiumAmt = calculateHabitPremium(insurancePremiumDetails.getHabits(), totalPremiumAmt);	
		
		
		return totalPremiumAmt;
	}
	
	private double getPremiumAmt(int n) {
		for(int i = 1; i <= n; i++) {
			if(i < 5) {
				basePremium = basePremium + ((basePremium*10)/100);
			} else {
				basePremium = basePremium + ((basePremium*20)/100);
			}
			
		}
		return basePremium;
	}
	
	private double calculateCurrentHealthPremium(CurrentHealth currentHealth, double totalPremiumAmt) {
		
		if(currentHealth.isHypertension()) {
			totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt)/100);
		}
		if(currentHealth.isBloodPressure()) {
			totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt)/100);
		}
		if(currentHealth.isBloodSugar()) {
			totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt)/100);
		}
		if(currentHealth.isOverweight()) {
			totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt)/100);
		}
		
		return totalPremiumAmt;
	}
	
	private double calculateHabitPremium(Habits habits, double totalPremiumAmt) {
		
		int percent = 0;
		
		if(habits.isDailyExercise()) {
			percent = percent - 3;
		}
		
		if(habits.isAlcohol()) {
			percent = percent + 3;
		}
		if(habits.isDrugs()) {
			percent = percent + 3;
		}
		if(habits.isSmoking()) {
			percent = percent + 3;			
		}
		if(percent != 0) {
			totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt*percent)/100);
		}
		return totalPremiumAmt;
	}

}

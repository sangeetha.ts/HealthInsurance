package com.health.insurance.test;
import static org.junit.Assert.*;

import org.junit.Test;

import com.health.insurance.model.CurrentHealth;
import com.health.insurance.model.Gender;
import com.health.insurance.model.Habits;
import com.health.insurance.model.InsurancePremiumDetails;
import com.health.insurance.service.InsurancePremiumService;
import com.health.insurance.service.InsurancePremiumServiceImpl;

public class Junit {

	@Test
	public void testForMale() {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		
		Habits habits = new Habits();
		CurrentHealth currentHealth = new CurrentHealth();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
	
		habits.setAlcohol(true);
		habits.setDailyExercise(true);
		
		currentHealth.setOverweight(true);
		
		details.setName("Norman Gomes");
		details.setGender(Gender.MALE);
		details.setAge(34);
		
		details.setCurrentHealth(currentHealth);
		details.setHabits(habits);
		
		assertEquals(6,856, insurancePremiumService.calculateInsurancePremium(details));
	}
	
	@Test
	public void testForFemale() {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		
		Habits habits = new Habits();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
		habits.setDailyExercise(true);
		
		details.setName("Anna");
		details.setGender(Gender.FEMALE);
		details.setAge(34);
		details.setHabits(habits);
		details.setCurrentHealth(new CurrentHealth());
		
		assertEquals(6,455, insurancePremiumService.calculateInsurancePremium(details));
	}
	
	@Test
	public void testForBelow18Female() {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
		
		details.setName("Anna");
		details.setGender(Gender.FEMALE);
		details.setAge(17);
		details.setCurrentHealth(new CurrentHealth());
		details.setHabits(new Habits());
		
		assertEquals(5,000, insurancePremiumService.calculateInsurancePremium(details));
	}
	
	@Test
	public void testForBelow18Male() {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
		
		details.setName("Anna");
		details.setGender(Gender.MALE);
		details.setAge(17);
		details.setCurrentHealth(new CurrentHealth());
		details.setHabits(new Habits());
		
		assertEquals(5,100, insurancePremiumService.calculateInsurancePremium(details));
	}
	
	@Test
	public void testForBelow25() {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
		
		details.setName("Anna");
		details.setGender(Gender.MALE);
		details.setAge(24);
		details.setCurrentHealth(new CurrentHealth());
		details.setHabits(new Habits());
		
		assertEquals(5,610, insurancePremiumService.calculateInsurancePremium(details));
	}
	
	@Test
	public void testForBelow35() {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
		
		details.setName("Anna");
		details.setGender(Gender.FEMALE);
		details.setAge(34);
		details.setCurrentHealth(new CurrentHealth());
		details.setHabits(new Habits());
		
		assertEquals(6,655, insurancePremiumService.calculateInsurancePremium(details));
	}
	
	@Test
	public void testForBelow30() {
		InsurancePremiumService insurancePremiumService = new InsurancePremiumServiceImpl();
		InsurancePremiumDetails details = new InsurancePremiumDetails();
		
		details.setName("Anna");
		details.setGender(Gender.FEMALE);
		details.setAge(29);
		details.setCurrentHealth(new CurrentHealth());
		details.setHabits(new Habits());
		
		assertEquals(6,050, insurancePremiumService.calculateInsurancePremium(details));
	}
	

}

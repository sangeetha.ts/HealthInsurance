package com.health.insurance.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class JunitRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(Junit.class);
		
      System.out.println("Number of failures : " + result.getFailureCount());
		
      System.out.println("Success  : " + result.wasSuccessful());
   }
} 